/*
* Fraction.hpp
*
*/

#include <string>

struct Fraction {
    //@{
    // Attributs membres
    int numerator = 0;
    int denominator = 1; // !!! Attention aux divisions par zéro
    //@}
    
    //@{
    //  Fonctions membres
    std::string display();
    void operator+(int i);
    void operator+(const Fraction a);
    bool operator<(const Fraction a);
    bool operator>(const Fraction a);
    //@}
};

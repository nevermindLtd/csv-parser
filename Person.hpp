/*
* Person.hpp
*
*/

#include <string>
#include "Fraction.hpp"

struct Person {
    //@{
    // Attributs membres
    std::string firstname;
    std::string lastname;
    std::string phoneNumber;
    Fraction answers;
    //@}
    
    //@{
    //  Fonctions membres
    bool hasValidPhoneNumber();
    bool hasValidNames();
    int getNumberOfPositiveResponses();
    int getNumberOfGivenResponses();
    //@}
};

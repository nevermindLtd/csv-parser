/*
 * PersonTest.cpp
 *
 */

#include "catch.hpp"
#include "Person.hpp"

using namespace std;

TEST_CASE("Test person has valid phone number", "[Person]") {
    Person bob;
    
    // Avec un num�ro valide
    bob.phoneNumber = "0643219401";
    REQUIRE( bob.hasValidPhoneNumber() );
    
    // Avec un num�ro invalide: 9 caract�res
    bob.phoneNumber = "012345678";
    REQUIRE( bob.hasValidPhoneNumber() == false );
    
    // Avec un num�ro invalide: pas de z�ro en d�but, mais 10 caract�res
    bob.phoneNumber = "1234567890";
    REQUIRE( bob.hasValidPhoneNumber() == false );

    // Avec un num�ro invalide: pas de z�ro en d�but et 9 caract�res
    bob.phoneNumber = "123456789";
    REQUIRE( bob.hasValidPhoneNumber() == false );
    
    // Avec un num�ro invalide: vide
    bob.phoneNumber = "";
    REQUIRE( bob.hasValidPhoneNumber() == false );

    // Avec un num�ro invalide: contenant au moins 1 caract�re alphab�tique
    bob.phoneNumber = "06A3219401";
    REQUIRE( bob.hasValidPhoneNumber() == false );
}

TEST_CASE("Test person has valid names", "[Person]") {
    Person bob;
    
    // Avec pr�nom et nom vides: invalide (l'�nonc� dit au moins 1 des 2)
    REQUIRE( bob.hasValidNames() == false );
    
    // Avec un pr�nom valide et un nom vide
    bob.firstname = "Louis-Arnaud";
    REQUIRE( bob.hasValidNames() );
    
    // + un nom valide
    bob.lastname = "D'Alan de Vichy";
    REQUIRE( bob.hasValidNames() );
    
    // Avec un pr�nom invalide
    bob.firstname = "Georges 5";
    REQUIRE( bob.hasValidNames() == false );
}

TEST_CASE("Test person get number of responses", "[Person]") {
    Person bob;
    
    // Avec des r�ponses
    bob.answers.numerator = 1;
    bob.answers.denominator = 2;
    
    REQUIRE( bob.getNumberOfPositiveResponses() == 1 );
    REQUIRE( bob.getNumberOfGivenResponses() == 2 );
}

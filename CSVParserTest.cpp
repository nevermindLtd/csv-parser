/*
 * CSVParserTest.cpp
 *
 */

#define CATCH_CONFIG_MAIN  // provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "CSVParser.hpp"

using namespace std;

TEST_CASE("Test init with file", "[CSVParser]") {
    CSVParser parser;
    REQUIRE( parser.initWithFile("sondage.csv") );
    REQUIRE( parser.numberOfColumns == 4 );
    REQUIRE( parser.numberOfRows == 20 );
    REQUIRE( parser.header.size() != 0 ); // vérifier que le vecteur est non vide
    REQUIRE( parser.header[0] == "Prénoms" );
    REQUIRE( parser.header[1] == "Numéros de téléphone" );
    REQUIRE( parser.header[2] == "Réponses" );
    REQUIRE( parser.header[3] == "Noms" );
}

TEST_CASE("Test get Line at rowNum", "[CSVParser]") {
    CSVParser parser;
    REQUIRE( parser.initWithFile("sondage.csv") );
    // Ligne 1
    vector<string> table1 = parser.getLine(1);
    REQUIRE( table1[0] == "Bastien" );
    REQUIRE( table1[1] == "0612345678" );
    REQUIRE( table1[2] == "4/18" );
    REQUIRE( table1[3] == "Righi" );
    // Dernière ligne
    vector<string> table20 = parser.getLine(20);
    REQUIRE( table20[0] == "Thomas" );
    REQUIRE( table20[1] == "0680138353" );
    REQUIRE( table20[2] == "14/15" );
    REQUIRE( table20[3] == "Colombier" );
    // Cas d'erreur avec rowNum en dehors des limites
    REQUIRE( parser.getLine(0).size() == 0 );
    REQUIRE( parser.getLine(21).size() == 0 );
}

TEST_CASE("Test get Line with highest rate", "[CSVParser]") {
    CSVParser parser;
    REQUIRE( parser.initWithFile("sondage.csv") );
    //
    vector<string> table1 = parser.getLineWithHighestRateOfResponses();
    REQUIRE( table1[0] == "Rémi" );
    REQUIRE( table1[1] == "0680737812" );
    REQUIRE( table1[2] == "20/20" );
    REQUIRE( table1[3] == "Berlioz" );
}

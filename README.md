# Tutoriel: CSVParser Struct

Ceci est une correction du `TD de POO Cpp - PolytechNice - 04-Structure-et-LecteurCSV.pdf` présent dans le répertoire.
Chaque **commit** correspond au code effectué pour répondre à une question du TD.

## Démarrer

* Cloner ce répertoire à partir de son URL dans le répertoire de votre choix sur votre ordinateur :
```
cd $HOME/Documents
git clone https://ange@bitbucket.org/ange/csv-parser.git
```
* Compiler le projet: `make`
* Exécuter les tests: `./test`

Ça marche !
